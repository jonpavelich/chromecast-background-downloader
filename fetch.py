import json
import logging
import re
import os
import pickle
import time

import requests

logging.basicConfig(level=logging.INFO)


def main():
    all_images = set()

    try:
        with open("statefile", "rb") as f:
            all_images = pickle.load(f)
            logging.info(
                f"Loaded state file, we are now aware of {len(all_images)} existing URLs"
            )
    except FileNotFoundError:
        logging.info("No state file found, starting without knowledge of existing URLs")

    while True:
        images = fetch()
        logging.info(f"Fetched {len(images)} image URLs")

        images = [x for x in images if x not in all_images]
        logging.info(f"Identified {len(images)} new image URLs")

        if len(images) == 0:
            logging.info(
                f"No new images fetched. We might be done. We will sleep then try again."
            )
            time.sleep(5)
            continue

        all_images |= set(images)
        logging.info(f"We are now aware of {len(all_images)} images in total")

        for link, source in images:
            if "gettyimages" in source:
                filename = source.split("/")[-2] + ".jpg"
            else:
                filename = f"UNTITLED-{link.split('/')[-1]}.jpg"

            if os.path.isfile(f"download/{filename}"):
                logging.warning(f"File exists, skipping: {filename}")
            else:
                logging.info(f"Fetching new image: {filename}")
                r = requests.get(link)
                with open(f"download/{filename}", "wb") as f:
                    f.write(r.content)

        with open("statefile", "wb") as f:
            pickle.dump(all_images, f)
        logging.info("Wrote updated state file")


def fetch():
    """Fetch the Google Chromecast Home page and parse out image URLs"""
    r = requests.get("https://clients3.google.com/cast/chromecast/home")
    match = re.search(r"JSON.parse\('(\\x5b[^\)]*)\)", r.text)
    text = match.group(1)
    text = re.sub(r"\\x5b", "[", text)
    text = re.sub(r"\\x5d", "]", text)
    text = re.sub(r"\\x22", '"', text)
    text = re.sub(r"\\\/", "/", text)
    text = re.sub(r"\\n", "\n", text)
    text = re.sub(r"\\\\u003d", "=", text)
    text = re.sub(r"'", "", text)
    j = json.loads(text)
    return [(x[0], x[9]) for x in j[0]]


if __name__ == "__main__":
    main()
