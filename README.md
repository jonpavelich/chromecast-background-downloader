# Chromecast Background Downloader

## What is this?

A small Python script that queries Google for the latest background images for Chromecasts, then downloads them. I created it because the Chromecast background images are beautiful and make great desktop wallpapers.

## Instructions

1. Ensure you have Python 3.7 or later.

2. Create and activate a virtual environment for the dependencies
   ```shell
   $ python -m venv venv
   $ source venv/bin/activate
   ```

3. Install the dependencies
   ```shell
   $ pip install -r requirements.txt
   ```

4. Create the download directory
   ```shell
   $ mkdir download
   ```

5. Run the program
   ```shell
   $ python fetch.py
   ```

## Known Issues
I wrote this as a very quick script to get the images I was after. As a result, it's pretty fragile - error handling is very poor and almost no testing has been done. Use this project at your own risk. Any changes on Google's side will break things. If you encounter any bugs please [open an issue](https://gitlab.com/jonpavelich/chromecast-background-downloader/issues/new) on GitLab so I can fix it.
